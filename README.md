## Clonar un repositorio

Utilice estos pasos para clonar desde SourceTree, nuestro cliente para usar la línea de comandos del repositorio gratis.
La clonación le permite trabajar en sus archivos localmente.
Si aún no tiene SourceTree, [descargue e instale primero] (https://www.sourcetreeapp.com/).
Si prefiere clonar desde la línea de comandos, consulte [Clonar un repositorio] (https://confluence.atlassian.com/x/4whODQ).

1. Verá el botón de clonar debajo del encabezado **Source**. Haz clic en ese botón.
2. Ahora haga clic en **Check out in SourceTree**. Es posible que deba crear una cuenta de SourceTree o iniciar sesión.
3. Cuando vea el cuadro de diálogo **Clone New** en SourceTree, actualice la ruta y el nombre de destino si lo desea y luego haga clic en **Clone**.
4. Abra el directorio que acaba de crear para ver los archivos de su repositorio.

Ahora que está más familiarizado con su repositorio de Bitbucket, continúe y agregue un nuevo archivo localmente.
Puede [enviar su cambio a Bitbucket con SourceTree] (https://confluence.atlassian.com/x/iqyBMg), o
puede [agregar, confirmar,] (https://confluence.atlassian.com/x/ 8QhODQ) y [pulsar desde la línea de comando] (https://confluence.atlassian.com/x/NQ0zDQ).
