import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.models';
@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {
  destinos : string[];
  constructor() {
    this.destinos = [];
    // this.destinos.push( new DestinoViaje( "Destino 1", "Url 1" ));
  }

  ngOnInit(): void {
  }

  guardar(nombre:string, url:string):boolean {
    this.destinos.push( new DestinoViaje(nombre,url));
    // console.log( this.destinos);
    return false;
  }

}
